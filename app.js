var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var app = express();


// // Middleware - are basically functions that have access to request and response objects 
// //and also access to the next piece of middleware thats gonna fire after it

// var logger = function(req, res, next){
// 	console.log('Logging...');
// 	next();
// }

// app.use(logger);



// Mapping the static assets directory - public
// where __dirname gives the apps root path 
// use the below as 
// http://localhost:5000/images/logo.png
//app.use(express.static(path.join(__dirname, 'public')));  
//app.use('/hathi', express.static('public'));

app.use(express.static('public')) 
// Use the below as
// http://localhost:5000/static/images/logo.png
//app.use('/static', express.static(path.join(__dirname, 'public')));  

 
 // Access a modular middleware
 var birds = require('./birds');
 app.use('/birds', birds); 

 //Handling a simple get request route 
app.get('/', function(req, res){
	res.send('Hello world!');	// Print out on the browser	
});

// Handling route parameters
// Access at http://localhost:5000/users/anyuserid
app.get('/users/:userId?', function(req, res){
	if(req.param('userId')){
		res.send('userId recieved as: ' + req.param('userId'));		
	}else{
		res.send('userId not recieved');		
	}
})

var cb0 = function (req, res, next) {
  console.log('CB0')
  next()
}

var cb1 = function (req, res, next) {
  console.log('CB1')
  next()
}

app.get('/example/d', [cb0, cb1], function (req, res, next) {
  console.log('the response will be sent by the next function ...')
  next()
}, function (req, res) {
  res.send('Hello from D!')
})

/*
// Handle independent logic as per request methods by chaining
//http:localhost:5000/routebyreqmethod by POSTMAN as per req method
app.route('/routebyreqmethod')
.get(function(req, res){
	res.send('GET request router');		
})
.post(function(req, res){
	res.send('POST request router');
})
.put(function(req, res){
	res.send('PUT request router');
});

var cb0 = function (req, res, next) {
	res.send('captured:' + req.params.ver);	
	if(req.params.ver == 1){
		res.send('v1 captured:' + req.params.ver);
		next();
	}
	else{
		next();
	}
}

var cb1 = function (req, res, next) {
	if(req.params.ver === 2){
		console.log('v2 captured:' + req.params.ver);
		return;		
	}
	else{
		next();
	}
}

var cb2 = function (req, res, next) {
	if(req.params.ver === 3){
		console.log('v3 captured:' + req.params.ver);
		return;		
	}
	else{
		next();
	}
}

app.get('/ver/:ver', [cb0, cb1, cb2]);
*/
// Handling routes with multiple callbacks
app.get('/ver/:ver', function (req, res, next) {
	if(req.params.ver == 1){
		res.send('V1 called')	
	}else{
		next();
	}  	  	

}, function (req, res, next) {
	if(req.params.ver == 2){
		res.send('V2 called')	
	}else{
		next();
	}  	 	 	

}, function(req, res, next){
	if(req.params.ver == 3){
		res.send('V3 called')	
	}else{
		next();
	}  	 	 	
})


// If a route is not found 
app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});

app.listen(5000, function(){
	console.log('Server started on port 5000...');
});